/* Config file for Parse Server instance. Here you can edit all the configuration variables of 
the server. */

module.exports = {
    IP: 'mongodb://localhost:27017/area',
    cloud_code: 'cloud/cloud_code.js',
    appId: 'Area Imprese',
    masterKey: '1234567',
    URL: 'http://138.68.100.112:3131/parse',
    port: '3131',
    name: 'Area Imprese',
    dashboard_port: '4041',
    user: 'Patrizio',
    pass: 'staffonly',
    readOnly: false,
    apps: [{'appId': 'Area Imprese'}]
}