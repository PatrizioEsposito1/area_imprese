//Principal web-app js page


/* requires block */
var Parse = require('parse');
var config = require('./config_client'); // <- client configuration
var express = require('express'); // i needed to re-declare express because i have to make REST API in this file


/* declaration block */

var app = express();

/* The code starts here 
// INITIALIZING PARSE CONNECTION INSTANCE */

Parse.initialize(config.masterKey, config.appId);
Parse.serverURL(config.serverURL);
Parse.User.enableUnsafeCurrentUser(); //This will make node.js enabled to use CurrentUser()


/* REST API Endpoints here - */

/* Signup endpoint, POST request with the params showed below. In case, I will create a little 
documentation with all the params needed for the request.
Anyway, this function should be already tested when the brief will come, so I will just add some 
params.*/
app.post('/user/signup', function(req, res){
    var name = req.body.name;
    var surname = req.body.surname;
    var phone = req.body.phone;
    var taxNumber = req.body.taxNumber;
    var birthday = req.body.birthday;
    var email = req.body.email;
    var password = req.body.password;
    var subscribedNews = req.body.subscribedNews; //this is for the agreement to newsletter
    var dataProcessing = req.body.dataProcessing; //this is for the agreement to data processing
    var dataCommunication = req.body.dataCommunication; //this is for the agreement to third-party data communication
    var role = req.body.role; //this is for user role, Operator or End User

    var user = new Parse.User();
    user.set("username", email); //the username will be the e-mail, or unique-generated SHA code(maybe with 6-7 characters slip)
    user.set("password", password);
    user.set("email", email);
    user.set("name", name);
    user.set("surname", surname);
    user.set("phone", phone);
    user.set("taxNumber", taxNumber);
    user.set("birthday", birthday);
    user.set("subscribedNews", subscribedNews);
    user.set("dataProcessing", dataProcessing);
    user.set("dataCommunication", dataCommunication);
    user.set("role", role);

    try{
        await user.signUp(); // all those calls in Parse API will be done asynchronously
        //need to make redirection
    }catch(parseError){
        console.log("Error: " + parseError.code + " " + parseError.message);
    }
});

/* 
......./////////////////////////          
           END SIGNUP
......./////////////////////////
*/


/* Login API endpoint
Same discourse as signup endpoint.
I will then add in a little documentation the calls to make in POST */

app.post('/user/login', function(req, res){
    var email = req.body.email;
    var password = req.body.password;
    const user = Parse.User.logIn(email, password);
    //need to make redirection
});

/* Lately I will make persistance recognizing session Id if the Parse.CurrentUser() won't work.
To do this, i will add two more packages: cookie-parse and cookie-session.
*/




/* Logout API endpoint */

app.get('/user/logout', function(req, res){
    Parse.User.logOut().then(() => {
          var user = Parse.User.current(); //setting the cached user to null
          //needs redirection
    });
});

