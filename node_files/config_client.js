/*  Client configuration file
This file configure the server-related variables for the client connection to it.
*/

module.exports = {
    appId: 'Bernardelli Assicurazioni',
    masterKey: '1234567',
    serverURL: 'http://138.68.100.112:1337/parse'
}