/* 
This file I created on 26/10/2018 needs to start the dashboard and the routing module
The reason behind is probably really tox syntax is the fact that i don't really care about this file,
as it is only for startup.
*/

//requirements block
var express = require("express");
var server = require("parse-server").ParseServer;
var dashboard = require("parse-dashboard");
var config = require("./config");

//declarements block
var app = express();


//starting server instance
var instance = new server(
    {
        databaseURI: config.IP, // Connection string for db
        cloud: config.cloud_code, // Absolute path to your Cloud Code
        appId: config.appId,
        masterKey: config.masterKey, // Remember to modify masterKey when you'll deploy the server
        serverURL: config.URL
    }
);

var params = { allowInsecureHTTP: true }; // I need this for http instead of https, for dev purposes

//starting dashboard instance
var dashboard_istance = new dashboard(
    {
        "apps":[
            {
                'serverURL': config.URL,
                'appId': config.appId,
                'masterKey': config.masterKey,
                'appName': config.name
            }
        ],
        "users":[
            {
                'user': config.user,
                'pass': config.pass,
                'readOnly': config.readOnly,
                'apps': config.apps
            }
        ]
    }, params);

//Making express serve /parse route for server instance
app.use('/parse', instance);
//Making express serve /dashboard route for dashboard instance
app.use('/dashboard', dashboard_istance);

//making ports listen to dashboard and server instance
app.listen(config.port, function(){
    console.log("Parse Server successfully configured on " + config.URL);
});

app.listen(config.dashboard_port, function(){
    console.log("Parse Dashboard successfully configured on port " + config.dashboard_port);
});
