import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import background from "assets/img/background-sidebar-login.jpg";
import logo from "assets/img/logo_login.png";
import loginRoutes from "routes/login.jsx";

var sidebar = "url("+background+")";
const style = {
    sidebar:{
        width:'30%',
        height: '100%',
        display:'flex',
        float:'left',
        background: sidebar
    },
    viewport:{
        width:'100vw',
        height:'100vh',
        overflow:'hidden'
    },
    logo:{
        position:'relative',
        left: 60,
        top: 60,
        width: 250
    },
    body:{
        width:'70%',
        height:'100%',
        display:'flex',
        float:'right',
        backgroundImage: 'linear-gradient(to right bottom, #4ac8ed, #24b8f9, #40a4ff, #748bf8, #a46ae2);'
    },
    container:{
        width:'40%',
        maxWidth:'340px',
        height:'auto',
        display:'block',
        position:'relative',
        top:'30%',
        margin:'0 auto',
        color:'white',
        textAlign:'left'
    }
}

const switchRoutes = (
    <Switch>
      {loginRoutes.map((prop, key) => {
        if (prop.redirect)
          return <Redirect from={prop.path} to={prop.to} key={key} />;
        return <Route path={prop.path} component={prop.component} key={key} />;
      })}
    </Switch>
  );

class Login extends React.Component{
    render(){
        return(
        <div className={this.props.classes.viewport}>
            {/* SIDEBAR */}
             <div className={this.props.classes.sidebar}>
             <div className={this.props.classes.logo}>
                 <img src={logo} style={{width:250}}></img>
             </div>
             </div>
            {/* BODY */}
             <div className={this.props.classes.body}>
                <div className={this.props.classes.container}>
                    {switchRoutes}
                </div>
             </div>

        </div>
            
        );
    }
}

export default withStyles(style)(Login);