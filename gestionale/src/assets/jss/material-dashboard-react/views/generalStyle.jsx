import { successColor } from "assets/jss/material-dashboard-react.jsx";
import { isAbsolute } from "path";
const generalStyle = {
    modalContainer:{
        position:"absolute"
    },
    modal:{
        position:"absolute"
    },
    modalBackdrop:{
        position:"absolute"
    }
};
export default generalStyle;