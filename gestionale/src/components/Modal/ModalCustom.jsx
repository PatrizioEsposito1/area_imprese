import React from "react";
import {Modal, Row, Col, FormControl} from "react-bootstrap";
import Button from "components/CustomButtons/Button.jsx";

//icon components
import PlusOne from "@material-ui/icons/PlusOne";


class ModalCustom extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.handleClose = this.handleClose.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleElementChange = this.handleElementChange.bind(this);
        this.buttonText = this.buttonText.bind(this);
        this.addChild = this.addChild.bind(this);

        this.state = {
          show: false,
          children: [],
          user: null
        };
      }
      
      handleClose() {
        this.setState({ show: false });
        this.props.closeCallback();
        this.props.resetCallback();
      }
    
      handleShow() {
        this.setState({ show: true });
        this.props.showCallback();
      }
      setBody(){
        this.setState({body: this.props.body})
      }
      addChild(){
        this.setState({children: this.state.children.concat({nome: '', 'cf': ''})});
      }
      handleElementChange(event){
        const target = event.target;
        const name = target.name;
        const value = target.value;

        this.setState({[name]:value});
        //console.log(this.state);
      }
      buttonText(){
        if(this.props.editMode){
          return "Salva modifiche";
        }else if(this.props.class === "Clienti"){
          return "Registra utente";
        }else if(this.props.class === "Comunicazione"){
          return "Aggiungi comunicazione";
        }else if(this.props.class === "Prodotto"){
          return "Aggiungi prodotto";
        }else if(this.props.class === "Crediti"){
          return "Aggiungi crediti";
        }
      }
    render(){
      {
        return(
        <div>
        {this.props.buttonName != null ? <Button color="success" onClick={this.handleShow}>
        {this.props.buttonName}
        </Button>:null}
        
        {this.props.editMode ? this.state.show=true: null}
        {this.props.crediti ? this.state.show=true: null}
        {
        this.state.show ?  <div>
      <Modal.Dialog className="overlay">
      <Modal.Header>
      <Modal.Title>{this.props.title}</Modal.Title>
      </Modal.Header>

      <Modal.Body>
      {this.props.body}
      {this.props.title==="Aggiungi Prodotto" ? 
      <b style={{padding:15, fontWeight:"bold", marginTop:10}}>N.B: La durata del servizio deve essere espressa in giorni</b>:null}
      </Modal.Body>

      <Modal.Footer>
      <Button color="danger" onClick={this.handleClose}>Chiudi</Button>
      <Button color="success" onClick={this.props.handleSubmit}>{this.buttonText()}</Button>
      </Modal.Footer>
      </Modal.Dialog>
      </div>
      :
      null
        }

     
      </div>
        );
      }
    }
}
export default ModalCustom;