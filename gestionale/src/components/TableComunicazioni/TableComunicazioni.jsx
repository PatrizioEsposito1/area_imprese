import React from "react";
import { Table, Button, FormControl } from "react-bootstrap";
import Parse from "parse";
import Edit from "@material-ui/icons/Edit";
import Clear from "@material-ui/icons/Clear";
import Popover from "@material-ui/core/Popover";

class TableUtenti extends React.Component{
      constructor(props, context){
          super(props, context);
          //Parse.User.logOut();
          this.getComunicazioni = this.getComunicazioni.bind(this);
          this.composeTable = this.composeTable.bind(this);
          this.removeComunicazioni = this.removeComunicazioni.bind(this);
          this.handleClick = this.handleClick.bind(this);
          this.handleClose = this.handleClose.bind(this);
          this.state={
              show: false,
              comunicazioni:[]
          };
      }
      handleClick(event){
          this.setState({
              target: event.currentTarget
          });
          //console.log(this.state);
      }
      handleClose(event){
          this.setState({
              target:null
          });

      }
      componentWillMount(){
          this.getComunicazioni();
      }
      getComunicazioni(){
          const comunicazioniQuery = new Parse.Query("Comunicazioni");
          comunicazioniQuery.find().then((Comunicazioni)=>{
              const results = [];
                for(var i = 0; i<Comunicazioni.length; i++){
                    results.push(Comunicazioni[i]);
                }
              this.setState({
                 ['comunicazioni']: results
              });
              //console.log(results);
          });
      }
      removeComunicazioni(event){
          var ComunicazioneStored = event.currentTarget.id;
          var ComunicazioneQuery = new Parse.Query("Comunicazioni");
          ComunicazioneQuery.get(ComunicazioneStored).then((Comunicazione)=>{
              Comunicazione.destroy({useMasterKey:true}).then((res)=>{
                alert("Comunicazione rimossa con successo");
                window.location.reload();
              }, (err)=>{
                alert("Comunicazione rimossa con successo");
                window.location.reload();
              });
          });
          
      }
      getExcerpt(corpo){
          var excerpt = corpo.substring(0, 80) + "..."; 
          return excerpt;
      }
      getAuthor(User){
          if(User != null){
            var Autore = JSON.parse(JSON.stringify(User));
            return Autore['nome'] + " " + Autore['cognome'];
          }
          
      }
      composeTable(){
          var rows = [];
          const comunicazioni = this.state.comunicazioni;
          console.log(comunicazioni);
            //Clienti
            rows.push(<thead><tr><th>Id</th><th>Titolo comunicazione</th><th>Corpo comunicazione</th><th>Autore</th><th colSpan="2">Azioni</th></tr></thead>);
            for(var i = 0; i<comunicazioni.length; i++){
                var comunicazione = comunicazioni[i];
                rows.push(<tr><td>{comunicazione.id}</td><td>{comunicazione.get('titolo')}</td><td>{this.getExcerpt(comunicazione.get('corpo'))}</td><td>{this.getAuthor(comunicazione.get('author'))}</td><td><Button bsStyle="link" id={comunicazione.id} onClick={this.props.edit}><Edit className="no-padding grey"/></Button>
                
                <Button bsStyle="link" id={comunicazione.id} onClick={this.removeComunicazioni}><Clear className="no-padding red"/></Button></td></tr>)
          }
          
          return <Table responsive>{rows}</Table>;
      }

      render(){
        const open = Boolean(this.state.target);
          return(
             <div>
            <Table>{this.composeTable()}</Table>
             </div>
          );
      }

}

export default TableUtenti;