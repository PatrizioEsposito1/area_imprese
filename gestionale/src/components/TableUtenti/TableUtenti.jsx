import React from "react";
import { Table, Button, FormControl, Row, Col, Grid } from "react-bootstrap";
import Parse from "parse";
import Edit from "@material-ui/icons/Edit";
import Clear from "@material-ui/icons/Clear";
import School from "@material-ui/icons/School";
import Popover from "@material-ui/core/Popover";
import EuroSymbol from "@material-ui/icons/EuroSymbol";
import Block from "@material-ui/icons/Block";
import ModalCustom from "components/Modal/ModalCustom.jsx";

const initial = null
class TableUtenti extends React.Component{
      constructor(props, context){
          super(props, context);
          //Parse.User.logOut();
          this.getUtenti = this.getUtenti.bind(this);
          this.composeTable = this.composeTable.bind(this);
          this.removeUtente = this.removeUtente.bind(this);
          this.handleClick = this.handleClick.bind(this);
          this.handleClose = this.handleClose.bind(this);
          this.blockUtente = this.blockUtente.bind(this);
          this.handleShow = this.handleShow.bind(this);
          this.handleElementChange = this.handleElementChange.bind(this);
          this.state={
              users: [],
              show: false
          };
      }
      handleClick(event){
          this.setState({
              target: event.currentTarget
          });
          //console.log(this.state);
      }
      handleClose(event){
          this.setState({
              target:null,
              show:false
          });
      }
      handleShow(event){
          this.setState({
              show:true,
              utenteCrediti:event.currentTarget.id
          });
          console.log("Clicked");
      }
      componentWillMount(){
          this.getUtenti();
          this.getTutors();
      }
      handleElementChange(event){
        const target = event.target;
        const name = target.name;
        const value = target.value;
    
        this.setState({[name]:value});
      }
      handleSubmit(event){
          const target = this.state.utenteCrediti;
          
          var UtenteQuery = new Parse.Query(Parse.User);
          console.log(target);
          UtenteQuery.get(target).then((Cliente)=>{
            var crediti = Cliente.get("crediti");
            Cliente.set('crediti', parseInt(crediti) + parseInt(this.state.crediti));
            Parse.Object.saveAll([Cliente], {useMasterKey:true}).then((res)=>{
                 alert("Crediti aggiunti con successo");
            });
          });
      }
      formComponents(){
        const components = [];
        const body = [];
        components.push(<Row className="show-grid"><Col xs={12} md={12}><h4>Inserisci importo crediti:</h4></Col></Row>);
        components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="crediti">Crediti:</label><FormControl name="crediti" onChange={this.handleElementChange} value={this.state.crediti}></FormControl></Col></Row>);
        body.push(<div><Grid>{components}</Grid></div>);
        return body;
      }
      getUtenti(){
          const UtentiQuery = new Parse.Query(Parse.User);
          UtentiQuery.equalTo("group", this.props.group);
          var result = [];
          const query = UtentiQuery.find().then((res)=>{ 
              const users = [];  
              res.forEach(function(user){
                 users.push(user);
              });
              result = users;
              this.setState({
                  ['users']: result
              });
          }, (err)=>{
               alert("Errore " + err.message);
          });
      }
      getTutors(){
          const UtentiQuery = new Parse.Query(Parse.User);
          UtentiQuery.equalTo("group", "3");
          const query = UtentiQuery.find().then((res)=>{
               const users = [];
               res.forEach(function(user){
                   users.push(<option value={user.id}>{user.get('nome') + " " + user.get('cognome')}</option>);
               });
               this.setState({
                   ['tutors']: users
               });
          }, (err)=>{
              alert("Errore " + err.message);
          });
      }
      removeUtente(event){
          var ClienteStored = event.currentTarget.id;
          var ClienteQuery = new Parse.Query(Parse.User);
          ClienteQuery.get(ClienteStored).then((Cliente)=>{
              Cliente.destroy({useMasterKey:true}).then((res)=>{
                alert("Utente rimosso con successo");

              }, (err)=>{
                alert("Utente rimosso con successo");
                Cliente.fetch();
              });
          });
          
      }
      blockUtente(event){
          var ClienteStored = event.currentTarget.id;
          var ClienteQuery = new Parse.Query(Parse.User);
          ClienteQuery.get(ClienteStored).then((Cliente)=>{
               if(Cliente.get("suspended")){
                   Cliente.set("suspended", false);
               }else{
                   Cliente.set("suspended", true);
               }
               Parse.Object.saveAll([Cliente], {useMasterKey:true}).then((res)=>{
                     alert("Utente aggiornato con successo");
                     window.location.reload();
               }, (err)=>{
                     alert("Errore " + err.message);
               });
          });
      }
      getTutor(user){
          var Tutor = user.get('tutor');
          try{
            if(Tutor != undefined){
                return (Tutor.get('nome') + " " + Tutor.get('cognome'));
              }else{
                  return null;
              }
          }catch(err){
              return null;
          }
          
      }
      composeTable(){
          var rows = [];
          const users = this.state.users;
          
          if(this.props.group === "4"){
            //Clienti
            rows.push(<thead><tr><th>Id</th><th>Nome e Cognome</th><th>Tutor</th><th>Crediti</th><th colSpan="2">Azioni</th></tr></thead>);
            for(var i = 0; i<users.length; i++){
                var user = users[i];
                rows.push(<tr><td>{user.id}</td><td>{user.get('nome') + " " + user.get('cognome')}</td><td>{this.getTutor(user)}</td><td>{user.get('crediti')}</td><td><Button bsStyle="link" id={user.id} onClick={this.props.edit}><Edit className="no-padding grey"/></Button>
                
                <Button bsStyle="link" onClick={this.handleClick} id={user.id}>
                <School className="no-padding grey"/>
                
                </Button>
                {
                    user.get('suspended') ? <Button bsStyle="link" id={user.id} onClick={this.blockUtente}><Block className="no-padding red"/></Button> : <Button bsStyle="link" id={user.id} onClick={this.blockUtente}><Block className="no-padding grey"/></Button>
                }
                <Button bsStyle="link" id={user.id} onClick={this.handleShow}><EuroSymbol className="no-padding grey"/></Button>
                
                <Button bsStyle="link" id={user.id} onClick={this.removeUtente}><Clear className="no-padding red"/></Button></td></tr>)
            }
          }else if(this.props.group === "3"){
            //Tutors
            rows.push(<thead><tr><th>Id</th><th>Nome e cognome</th><th>E-mail</th><th>Azioni</th></tr></thead>);
            for(var i = 0; i<users.length; i++){
                var user = users[i];
                rows.push(<tr><td>{user.id}</td><td>{user.get('nome') + " " + user.get('cognome')}</td><td>{user.get('username')}</td><td><Button bsStyle="link" id={user.id} onClick={this.props.edit}><Edit className="no-padding grey"/></Button><Button bsStyle="link" id={user.id} onClick={this.removeUtente}><Clear className="no-padding red"/></Button></td></tr>);
            }

          }else if(this.props.group === "2"){
            //Operatori
            rows.push(<thead><tr><th>Id</th><th>Nome e cognome</th><th>E-mail</th><th>Mansione</th><th>Azioni</th></tr></thead>);
            for(var i = 0; i<users.length;i++){
                var user = users[i];
                rows.push(<tr><td>{user.id}</td><td>{user.get('nome') + " " + user.get('cognome')}</td><td>{user.get('username')}</td><td>{user.get('mansione')}</td><td><Button bsStyle="link" id={user.id} onClick={this.props.edit}><Edit className="no-padding grey"/></Button><Button bsStyle="link" id={user.id} onClick={this.removeUtente}><Clear className="no-padding red"/></Button></td></tr>);
            }
          }
          
          return <Table responsive>{rows}</Table>;
      }

      selectTutor(event){
          var TutorQuery = new Parse.Query(Parse.User);
          TutorQuery.get(event.currentTarget.value).then((Tutor)=>{
              var selectedTutor = Tutor;
              var target = this.state.target;
              TutorQuery.get(target.id).then((Cliente)=>{
                  Cliente.set('tutor', selectedTutor);
                  Parse.Object.saveAll([Cliente], {useMasterKey:true}).then((res)=>{
                    alert("Tutor impostato con successo");
                  });
              });
          });
      }
      closeCallback(){
        this.setState({
          edit:false,
          show:false
        });
      }
    
      resetState(){
        this.state = initial;
      }
      render(){
        const open = Boolean(this.state.target);
          return(
             <div>
             <ModalCustom buttonName={null} title="Aggiungi Crediti" class="Crediti" 
      body={this.formComponents()} 
      handleChange={this.handleElementChange.bind(this)} 
      handleSubmit={this.handleSubmit.bind(this)} 
      editMode={null}
      closeCallback={this.closeCallback.bind(this)} 
      showCallback={this.handleShow.bind(this)}
      userId={null} 
      resetCallback={this.resetState.bind(this)}
      crediti={this.state.show}
      />
            <Table>{this.composeTable()}</Table>
            
             <Popover
          id="simple-popper"
          open={open}
          anchorEl={this.state.target}
          onClose={this.handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          style={{padding:15}}
        >
        
          <FormControl componentClass="select" placeholder="select" name="tutor" onChange={this.selectTutor.bind(this)} style={{width:200}}>
              <option>Scegli tutor</option>
              {this.state.tutors}
          </FormControl>
        </Popover>
        
        </div>
          );
      }

}

export default TableUtenti;