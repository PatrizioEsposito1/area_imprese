import React from "react";
import { Table, Button, FormControl } from "react-bootstrap";
import Parse from "parse";
import Edit from "@material-ui/icons/Edit";
import Clear from "@material-ui/icons/Clear";
import Popover from "@material-ui/core/Popover";

class TableUtenti extends React.Component{
      constructor(props, context){
          super(props, context);
          //Parse.User.logOut();
          this.getProdotti = this.getProdotti.bind(this);
          this.composeTable = this.composeTable.bind(this);
          this.removeProdotto = this.removeProdotto.bind(this);
          this.handleClick = this.handleClick.bind(this);
          this.handleClose = this.handleClose.bind(this);
          this.state={
              show: false,
              prodotti:[]
          };
      }
      handleClick(event){
          this.setState({
              target: event.currentTarget
          });
          //console.log(this.state);
      }
      handleClose(event){
          this.setState({
              target:null
          });

      }
      componentWillMount(){
          this.getProdotti();
      }
      getProdotti(){
          const prodottiQuery = new Parse.Query("Prodotti");
          prodottiQuery.find().then((Prodotti)=>{
              const results = [];
                for(var i = 0; i<Prodotti.length; i++){
                    results.push(Prodotti[i]);
                }
              this.setState({
                 ['prodotti']: results
              });
              //console.log(results);
          });
      }
      removeProdotto(event){
          var ProdottoStored = event.currentTarget.id;
          var ProdottiQuery = new Parse.Query("Prodotti");
          ProdottiQuery.get(ProdottoStored).then((Prodotto)=>{
              Prodotto.destroy({useMasterKey:true}).then((res)=>{
                alert("Prodotto rimosso con successo");
                window.location.reload();
              }, (err)=>{
                alert("Prodotto rimosso con successo");
                window.location.reload();
              });
          });
          
      }
      getExcerpt(corpo){
          if(corpo != undefined){
            var excerpt = corpo.substring(0, 80) + "..."; 
            return excerpt;
          }else{
              return "";
          }

      }
      composeTable(){
          var rows = [];
          const prodotti = this.state.prodotti;
          console.log(prodotti);
            //Clienti
            rows.push(<thead><tr><th>Id</th><th>Nome prodotto</th><th>Descrizione prodotto</th><th>Durata</th><th>Prezzo</th><th colSpan="2">Azioni</th></tr></thead>);
            for(var i = 0; i<prodotti.length; i++){
                var prodotto = prodotti[i];
                rows.push(<tr><td>{prodotto.id}</td><td>{prodotto.get('nome')}</td><td>{this.getExcerpt(prodotto.get('descrizione'))}</td><td>{prodotto.get('durata')}</td><td>{prodotto.get('prezzo')}</td><td><Button bsStyle="link" id={prodotto.id} onClick={this.props.edit}><Edit className="no-padding grey"/></Button>
                <Button bsStyle="link" id={prodotto.id} onClick={this.removeProdotto}><Clear className="no-padding red"/></Button></td></tr>)
          }
          
          return <Table responsive>{rows}</Table>;
      }

      render(){
        const open = Boolean(this.state.target);
          return(
             <div>
            <Table>{this.composeTable()}</Table>
             </div>
          );
      }

}

export default TableUtenti;