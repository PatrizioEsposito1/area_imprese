import React from "react";
import {Modal, Row, Col, FormControl} from "react-bootstrap";
import Button from "components/CustomButtons/Button.jsx";

//icon components
import PlusOne from "@material-ui/icons/PlusOne";


class ModalCustom extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.handleClose = this.handleClose.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleElementChange = this.handleElementChange.bind(this);
        this.buttonText = this.buttonText.bind(this);

        this.state = {
          show: false,
          children: [],
          user: null
        };
      }
      
      handleClose() {
        this.setState({ show: false });
        this.props.closeCallback();
        this.props.resetCallback();
      }
    
      handleShow() {
        this.setState({ show: true });
        this.props.showCallback();
      }
      setBody(){
        this.setState({body: this.props.body})
      }
      handleElementChange(event){
        const target = event.target;
        const name = target.name;
        const value = target.value;

        this.setState({[name]:value});
        //console.log(this.state);
      }
      buttonText(){
        return "Aggiungi Crediti";
      }
    render(){
      {
        return(
        <div>
        {this.props.buttonName != null ? <Button color="success" onClick={this.handleShow}>
        {this.props.buttonName}
        </Button>:null}
       <div>
      <Modal.Dialog className="overlay">
      <Modal.Header>
      <Modal.Title>{this.props.title}</Modal.Title>
      </Modal.Header>

      <Modal.Body>
      {this.props.body}
      </Modal.Body>

      <Modal.Footer>
      <Button color="danger" onClick={this.handleClose}>Chiudi</Button>
      <Button color="success" onClick={this.props.handleSubmit}>{this.buttonText()}</Button>
      </Modal.Footer>
      </Modal.Dialog>
      </div>
      </div>
        );
      }
    }
}
export default ModalCustom;