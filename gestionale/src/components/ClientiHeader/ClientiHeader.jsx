import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import withStyles from "@material-ui/core/styles/withStyles";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography";
import Parse from "parse";

const style = {
     rightAlign: {
         float: 'right',
         textAlign: 'right',
         marginLeft: '80%'
     }
}
class ClientiHeader extends React.Component{
     constructor(props, context){
         super(props, context);
         this.state = {};
     }

     getRoute = ()=>{
         const path = window.location.href;
         const route = path.split('/')[3];
         return route.charAt(0).toUpperCase() + route.slice(1);
     }


     render(){
         return(
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton color="inherit" aria-label="Menu">
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            {this.getRoute()}
                        </Typography>
                        <Typography variant="h6" className={this.props.classes.rightAlign}>
                            <b className={this.props.classes.textBold}>
                                Crediti:
                            </b>
                            { Parse.User.current().get('crediti') }
                        </Typography>
                    </Toolbar>
                </AppBar>
            </div>
         );
     }
}

export default withStyles(style)(ClientiHeader)