import React from "react";
import { FormControl, Row, Col, Grid } from 'react-bootstrap';
import withStyles from "@material-ui/core/styles/withStyles";
import Button from '@material-ui/core/Button';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import theme from "assets/jss/material-dashboard-react/loginTheme.jsx";
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import SwipeableViews from 'react-swipeable-views';
import { Redirect } from 'react-router-dom';

//Parse
import Parse from "parse";

const style={
     inputLabel:{
         color:'white'
     },
     input:{
         padding:"9px 20px !important",
         height:"auto !important",
         border:0,
         marginTop:10
     },
     forgot:{
         color:'white',
         marginLeft:4
     },
     footerForm:{
         marginTop:15,
         borderTop:'1px solid white',
         paddingTop:15
     },
     white:{
         color:'white',
         border:'1px solid white'
     }
}

class Login extends React.Component{
    constructor(props, context){
        super(props, context);
        this.handleChange = this.handleChange.bind(this);
        Parse.User.logOut();
        this.state={
            value: 0,
            redirect: false
        }
    }
    
    Login = (event)=>{
        var username = this.state.username;
        var password = this.state.password;

        Parse.User.logIn(username, password).then((res)=>{
              //success
              this.setRedirect();
        }, (err)=>{
            if(err.code == 101){
                alert("Campo Password o Email errato. Riprovare.");
            }else{
                console.log(err);
            }
        });
    }

    Subscribe = (event) =>{
        var username = this.state.username;
        var password = this.state.password;
        var conferma = this.state.conferma;
        if(username !== undefined && password !== undefined && conferma !== undefined){
            if(password !== conferma){
                alert("Le password non corrispondono");
            }else{
                var User = new Parse.User;
                User.set('username', username);
                User.set('password', password);
                User.set('group', "4");
                User.set('crediti', 0);
                User.signUp().then((res)=>{
                    this.setRedirect();
                }, (err)=>{
                    alert(err.message);
                });
            }
        }else{
            alert("Tutti i campi sono obbligatori");
        }
        
    }
    handleChange = (event, value) => {
        this.setState({ value });
    };
    handleChangeIndex = index => {
        this.setState({ value: index });
    };
    handleElementChange = (event)=>{
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    setRedirect = () => {
        this.setState({
            redirect: true
        })
    }
    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/dashboard' />
        }
      }
    render(){
        return(
            <div>
            {this.renderRedirect()}

            <MuiThemeProvider theme={theme}>
            <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            fullWidth
            >
            <Tab label="Accedi" />
            <Tab label="Registrati" />
          </Tabs>
          <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={this.state.value}
          onChangeIndex={this.handleChangeIndex}
        >
        <div>
        <h3 style={{fontWeight:'bold', marginBottom:'5px'}}>Accedi ora alla dashboard.</h3>
            <p>Benvenuto nel nostro nuovo servizio di amministrazione.</p>
            <FormControl name="username" placeholder="E-mail" onChange={this.handleElementChange} className={this.props.classes.input}></FormControl>
            <FormControl name="password" type="password" placeholder="Password" onChange={this.handleElementChange} className={this.props.classes.input}></FormControl>
            <div className={this.props.classes.footerForm}>
            <Button variant="outlined" onClick={this.Login} className={this.props.classes.white}>Accedi</Button><Button variant="text" className={this.props.classes.forgot}>Dimenticato la password?</Button>
            </div>
            </div>
        <div>
        <h3 style={{fontWeight:'bold', marginBottom:'5px'}}>Registrati ora alla dashboard.</h3>
            <p>Ti aspettano numerose funzioni a misura di utente.</p>
            <FormControl name="username" placeholder="E-mail" onChange={this.handleElementChange} className={this.props.classes.input}></FormControl>
            <FormControl name="password" type="password" placeholder="Password" onChange={this.handleElementChange} className={this.props.classes.input}></FormControl>
            <FormControl name="conferma" type="password" placeholder="Conferma password" onChange={this.handleElementChange} className={this.props.classes.input}></FormControl>
            <div className={this.props.classes.footerForm}>
            <Button onClick={this.Subscribe} variant="outlined" className={this.props.classes.white}>Registrati</Button>
            </div>
            </div>
            </SwipeableViews>
            </MuiThemeProvider>
            </div>
        );
    }

}

export default withStyles(style)(Login);