import React from "react";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import { FormControl, Row, Col, Grid } from 'react-bootstrap';
import generalStyle from "assets/jss/material-dashboard-react/views/generalStyle.jsx";
import ModalCustom from "components/Modal/ModalCustom.jsx";
import TableProdotti from "components/TableProdotti/TableProdotti.jsx";

//Parse components
import Parse from "parse";
import { runInThisContext } from "vm";

const initial = {
  show:false,
  edit:false,
  user:null
}
class Prodotti extends React.Component{
  constructor(props, context){
    super(props, context);
    this.handleShow = this.handleShow.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.formComponents = this.formComponents.bind(this);
    this.handleElementChange = this.handleElementChange.bind(this);
    this.getOperatori = this.getOperatori.bind(this);
    this.handleOperatoreChange = this.handleOperatoreChange.bind(this);
    this.state = {
      show: false,
      edit: false,
      user: null,
      assign: false,
      operatori: [],
      operatore:null
    };
  }
  
  handleElementChange(event){
    const target = event.target;
    const name = target.name;
    const value = target.value;

    this.setState({[name]:value});
  }
  getOperatori(){
    const OperatoriQuery = new Parse.Query(Parse.User);
    OperatoriQuery.equalTo("group", "2");
    OperatoriQuery.find().then((Operatori)=>{
      var OperatoriArray = []
        for(var i = 0; i<Operatori.length; i++){
          var Operatore = Operatori[i];
          OperatoriArray.push(<option value={Operatore.id}>{Operatore.get("nome") + " " + Operatore.get("cognome")}</option>);
         // console.log(OperatoriArray);
        }
        this.setState({
          ["operatori"]: OperatoriArray
        });
    });
  }
  handleOperatoreChange(event){
    var OperatoreQuery = new Parse.Query(Parse.User);
    OperatoreQuery.get(event.currentTarget.value).then((Operatore)=>{
      //console.log(Operatore);
      this.setState({
        ["operatore"]:Operatore
      });
    });
    
  }
  formComponents(){
    const components = [];
    const body = [];
    components.push(<Row className="show-grid"><Col xs={12} md={12}><h4>Dettagli prodotto:</h4></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="nome">Nome prodotto:</label><FormControl name="nome" onChange={this.handleElementChange} value={this.state.nome}></FormControl></Col>{/*<Col md={6} xs={6}><label htmlFor="operatore">Operatore</label>
    <FormControl componentClass="select" onChange={this.handleOperatoreChange} >
      <option>Scegli l'operatore</option>
      {this.state.operatori}
    </FormControl></Col>*/}</Row>);
    components.push(<Row className="show-grid"><Col xs={12} md={12}><label htmlFor="descrizione">Descrizione prodotto:</label><FormControl name="descrizione" componentClass="textarea" onChange={this.handleElementChange} value={this.state.descrizione} style={{height:200}}></FormControl></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="durata">Durata prodotto</label><FormControl name="durata" onChange={this.handleElementChange} value={this.state.durata}></FormControl></Col><Col xs={6} md={6}><label>Prezzo prodotto:</label><FormControl name="prezzo" onChange={this.handleElementChange} value={this.state.prezzo}></FormControl></Col></Row>)
    
    body.push(<div><Grid>{components}</Grid></div>);
    return body;
  }
  componentDidUpdate(){
   // console.log(this.state);
    if(this.state.nome != null && !this.state.edit && !this.state.show){
      for(var key in this.state){
        if(key != "show" && key != "edit"){
          this.setState({[key]: null});
          this.getOperatori();
        }
      }
    }

  }
  handleClose() {
    this.setState({
      show:false
    });
    ModalCustom.handleClose();
  }

  handleShow() {
    this.setState({
       show:true
    });
  }

  closeCallback(){
    this.setState({
      edit:false,
      show:false
    });
  }

  resetState(){
    this.state = initial;
  }
  
  componentWillMount(){
    this.getOperatori();
  }

  componentDidMount(){
    this.getOperatori();
  }
  toggleEdit(event){
    var id = event.currentTarget.id;
    this.setState({
      edit: true,
      userId: id
    });
    var ProdottiQuery = new Parse.Query("Prodotti");
    ProdottiQuery.get(id).then((Prodotto)=>{
      var ProdottoJSON = JSON.parse(JSON.stringify(Prodotto));
      //console.log(this.state);
      for(var key in ProdottoJSON){
        if(key == "operatore"){
          this.setState({
            [key]:ProdottoJSON[key]['objectId']
          });
          console.log(this.state);
        }else{
          this.setState({
            [key]:ProdottoJSON[key]
          });
        }
        

      }
    });
  }
  handleSubmit(){
    if(this.state.edit == false){
      var ProdottiClass = Parse.Object.extend("Prodotti");
      var Prodotto = new ProdottiClass();
      for(var key in this.state){
        if(key != "show" && key != "edit" && key != "user" && key != "operatori"){
          Prodotto.set(key, this.state[key]);
        }
      }
      
      Parse.Object.saveAll([Prodotto], {useMasterKey:true}).then((obj)=>{
          alert("Prodotto " + this.state.name + " aggiunto con successo");
          window.location.reload();
          
      }, (err)=>{
          alert("Errore " + err.message);
      }); 
    }else{
      const prodottoId = this.state.userId;
      const ProdottoQuery = new Parse.Query("Prodotti");
      if(typeof this.state.operatore === "string"){
        const OperatoreQuery = new Parse.Query(Parse.User);
        OperatoreQuery.get(this.state.operatore).then((Operatore)=>{
          this.setState({
            'operatore': Operatore
          });
        });
      }
      ProdottoQuery.get(prodottoId).then((Prodotto)=>{
          for(var key in this.state){
            if(key != "show" && key != "edit" && key != "sessionToken" && key != "operatori"){
              Prodotto.set(key, this.state[key]);
            }
          }
          Parse.Object.saveAll([Prodotto], {useMasterKey:true}).then((res)=>{
               alert("Prodotto modificato con successo");
          });
      });
    }
    
  }

  render(){
    return (
        <div>
        <ModalCustom buttonName="Aggiungi Prodotto" title="Aggiungi Prodotto" class="Prodotto" 
      body={this.formComponents()} 
      handleChange={this.handleElementChange.bind(this)} 
      handleSubmit={this.handleSubmit.bind(this)} 
      editMode={this.state.edit} 
      closeCallback={this.closeCallback.bind(this)} 
      showCallback={this.handleShow.bind(this)}
      userId={this.state.userId} 
      resetCallback={this.resetState.bind(this)}
      />
        <Card>
      <CardHeader color="primary">
           <h3 className="Utenti">Prodotti</h3>
      </CardHeader>
      <CardBody>
      <TableProdotti edit={this.toggleEdit.bind(this)} submit={this.handleSubmit}/>
      </CardBody>
    </Card>
        </div>

    );
  }
}


export default withStyles(generalStyle)(Prodotti);
