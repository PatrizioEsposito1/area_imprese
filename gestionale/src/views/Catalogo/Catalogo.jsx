import React from "react";

import Parse from "parse";
import withStyles from "@material-ui/core/styles/withStyles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import Button from "@material-ui/core/Button";

const styles = theme => ({
      root:{
          width:'100%',
          marginTop: theme.spacing.unit * 3,
          overflowX: 'auto',
          padding: 15
      },
      table:{
          padding: 10
      }
})


class Catalogo extends React.Component{
    
    constructor(props, context){
        super(props, context);

        this.state = {

        }
    }
    componentDidMount = () =>{
         var ProdottiQuery = new Parse.Query("Prodotti");
         ProdottiQuery.find().then((Prodotti)=>{
             //console.log(Prodotti);
             this.setState({
                 "prodotti": Prodotti
             });
         });
    }

    renderProdotti = () => {
        const rows = [];
        const prodotti = this.state.prodotti;
        if(prodotti != undefined){
        for(var i = 0; i<prodotti.length; i++){
           rows.push(
               <TableRow>
               <TableCell><b className={this.props.classes.boldText}>{prodotti[i].get('nome')}</b></TableCell>
               <TableCell>{prodotti[i].get('descrizione')}</TableCell>
               <TableCell>{prodotti[i].get('prezzo')}</TableCell>
               <TableCell><Button variant="contained" id={prodotti[i].id} onClick={this.acquistaProdotto}>Compra ora</Button></TableCell>
               </TableRow>
           )
        }
        return rows;
      }
    }
    acquistaProdotto = (event) => {
        const target = event.currentTarget;
        var prodottoQuery = new Parse.Query('Prodotti');
        prodottoQuery.get(target.id).then((Prodotto)=>{
            var crediti = Parse.User.current().get('crediti');
            var prezzo = Prodotto.get('prezzo');

            if(prezzo < crediti){
                crediti -= parseInt(Prodotto.get('prezzo'));
            }else{
                return alert('Crediti insufficienti. Acquistane altri per poter ordinare il servizio.');
            }

            Parse.User.current().set('crediti', crediti);
            Parse.User.current().save().then((res)=>{
                var Ordine = Parse.Object.extend('Ordini');
                var ordine = new Ordine();
                ordine.set('cliente', Parse.User.current());
                ordine.set('prodotto', Prodotto);
                Parse.Object.saveAll([ordine], {useMasterKey: true}).then((res)=>{
                    alert("Prodotto acquistato con successo");
                    window.location.reload();
                });
            });
        });
    }
    render(){
        return(
            <div>
               <h2>Lista prodotti</h2>
               <Paper className={this.props.classes.root}>
               <Table className={this.props.classes.table}>
                   <TableHead>
                       <TableRow>
                           <TableCell>Nome prodotto</TableCell>
                           <TableCell>Descrizione</TableCell>
                           <TableCell>Prezzo</TableCell>
                           <TableCell>Azioni</TableCell>
                       </TableRow>
                   </TableHead>
                   <TableBody>
                       {this.renderProdotti()}
                   </TableBody>
               </Table>
               </Paper>
            </div>
        );
    }
}

export default withStyles(styles)(Catalogo);