import React from "react";
import ReactDOM from 'react-dom';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import { FormControl, Row, Col, Grid } from 'react-bootstrap';
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import generalStyle from "assets/jss/material-dashboard-react/views/generalStyle.jsx";
import ModalCustom from "components/Modal/ModalCustom.jsx";
import TableUtenti from "components/TableUtenti/TableUtenti.jsx";

//Parse
import Parse from "parse";

const initial = {
  show: false,
  edit: false,
  user: null
}

class Clienti extends React.Component{
  
  constructor(props, context) {
    super(props, context);
    //this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.formComponents = this.formComponents.bind(this);
    this.handleElementChange = this.handleElementChange.bind(this);
    this.state = {
      show: false,
      edit: false,
      user: null,
      assign: false
    };
  }
  
  handleElementChange(event){
    const target = event.target;
    const name = target.name;
    const value = target.value;

    this.setState({[name]:value});
  }

  handleClose() {
    this.setState({
      show:false
    });
    ModalCustom.handleClose();
  }

  handleShow() {
    this.setState({
       show:true
    });
  }
  
  handleSubmit(){
    if(this.state.edit == false){
      var ClienteObj = Parse.Object.extend("_User");
      var Cliente = new ClienteObj();
      for(var key in this.state){
        if(key !== "show" && key !== "edit"){
          Cliente.set(key, this.state[key]);
        }
      }
      Cliente.set("username", this.state.username);
      Cliente.set("group", "4");
      Cliente.set("crediti", 0);
      Cliente.set("password", this.state.password);
      Parse.Object.saveAll([Cliente],{useMasterKey:true}).then((obj)=>{
          alert("Cliente " + this.state.nome + " " + this.state.cognome + " registrato con successo");
          window.location.reload();
          
      }, (err)=>{
          alert("Errore " + err.message);
      }); 
    }else{
      const storedUser = this.state.userId;
      console.log(storedUser);
      const ClienteQuery = new Parse.Query(Parse.User);
      ClienteQuery.get(storedUser).then((Cliente)=>{
          console.log(Cliente);
          for(var key in this.state){
            if(key !== "show" && key !== "edit" && key !== "sessionToken" && key !== "tutor"){
              Cliente.set(key, this.state[key]);
            }
          }
          Parse.Object.saveAll([Cliente], {useMasterKey:true}).then((res)=>{
               alert("Utente modificato con successo");
          });
      });
    }
    
  }
  resetState(){
    this.state = initial;
  }
  formComponents(){
    const components = [];
    const body = [];
    components.push(<Row className="show-grid"><Col xs={12} md={12}><h4>Rappresentante Legale:</h4></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="nome">Nome:</label><FormControl name="nome" onChange={this.handleElementChange} value={this.state.nome}></FormControl></Col><Col xs={6} md={6}><label htmlFor="cognome">Cognome</label><FormControl name="cognome" onChange={this.handleElementChange} value={this.state.cognome}></FormControl></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="email">E-Mail:</label><FormControl name="username" onChange={this.handleElementChange} value={this.state.username}></FormControl></Col><Col xs={6} md={6}><label htmlFor="password">Password:</label><FormControl name="password" type="password" onChange={this.handleElementChange} value={this.state.password}></FormControl></Col></Row>)
    components.push(<Row className="show-grid"><Col xs={12} md={8}><label htmlFor="indirizzo">Indirizzo:</label><FormControl name="indirizzo" onChange={this.handleElementChange} value={this.state.indirizzo}></FormControl></Col><Col xs={6} md={4}><label htmlFor="CAP">CAP:</label><FormControl name="CAP" onChange={this.handleElementChange} value={this.state.CAP}></FormControl></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="telefono">Telefono:</label><FormControl name="telefono" onChange={this.handleElementChange} value={this.state.telefono}></FormControl></Col><Col xs={6} md={6}><label htmlFor="cellulare">Cellulare:</label><FormControl name="cellulare" onChange={this.handleElementChange} value={this.state.cellulare}></FormControl></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={12} md={12}><h4>Scheda azienda:</h4></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={8} md={8}><label htmlFor="denominazione">Denominazione</label><FormControl name="denominazione" onChange={this.handleElementChange} value={this.state.denominazione}></FormControl></Col><Col xs={4} md={4}><label htmlFor="cod_ateco">Cod.Ateco</label><FormControl name="cod_ateco" onChange={this.handleElementChange} value={this.state.cod_ateco}></FormControl></Col></Row>)
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="attivita">Attività:</label><FormControl name="attivita" onChange={this.handleElementChange} value={this.state.attivita}></FormControl></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="p_iva">C.F/P.IVA:</label><FormControl name="p_iva" onChange={this.handleElementChange} value={this.state.p_iva}></FormControl></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={12} md={12}><label htmlFor="sede_legale">Sede Legale:</label><FormControl name="sede_legale" onChange={this.handleElementChange} value={this.state.sede_legale}></FormControl></Col></Row>)
    components.push(<Row className="show-grid"><Col xs={12} md={12}><label htmlFor="sede_operativa">Sede Operativa:</label><FormControl name="sede_operativa" onChange={this.handleElementChange} value={this.state.sede_operativa}></FormControl></Col></Row>)
    components.push(<Row className="show-grid"><Col xs={4} md={4}><label htmlFor="azienda_tel">Tel.</label><FormControl name="azienda_tel" onChange={this.handleElementChange} value={this.state.azienda_tel}></FormControl></Col><Col xs={4} md={4}><label for="azienda_fax">Fax</label><FormControl name="azienda_fax" onChange={this.handleElementChange} value={this.state.azienda_fax}></FormControl></Col><Col xs={4} md={4}><label for="azienda_cell">Cell.</label><FormControl name="azienda_cell" onChange={this.handleElementChange} value={this.state.azienda_cell}></FormControl></Col></Row>)
    components.push(<Row className="show-grid"><Col xs={4} md={4}><label htmlFor="numero_dipendenti">Numero dipendenti</label><FormControl name="azienda_dipendenti" onChange={this.handleElementChange} value={this.state.azienda_dipendenti}></FormControl></Col></Row>)
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="scontistica">Scontistica dedicata:</label><FormControl name="scontistica" onChange={this.handleElementChange} value={this.state.scontistica}></FormControl></Col> </Row>)
    /*components.push(<Row className="show-grid"><Col xs={12} md={12}><h4 style={{paddingTop:20}}>Dati coniuge</h4></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="nome_coniuge">Nome:</label><FormControl name="nome_coniuge" onChange={this.handleElementChange} value={this.state.nome_coniuge}></FormControl></Col><Col xs={6} md={6}><label for="cognome_coniuge">Cognome coniuge:</label><FormControl name="cognome_coniuge" onChange={this.handleElementChange} value={this.state.cognome_coniuge}></FormControl></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="attivita_coniuge">Attività:</label><FormControl name="attivita_coniuge" onChange={this.handleElementChange} value={this.state.attivita_coniuge}></FormControl></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={12} md={12}><label htmlFor="altri_redditi_coniuge">Altri redditi:</label><FormControl name="altri_redditi_coniuge" componentClass="textarea" onChange={this.handleElementChange} value={this.state.altri_redditi_coniuge}></FormControl></Col></Row>)
    components.push(<Row className="show-grid"><Col xs={12} md={12}><h4 style={{paddingTop:20}}>Figli a carico</h4></Col></Row>);*/
    body.push(<div><Grid>{components}</Grid></div>);
    return body;
  }
  
  assignTutor(){
    const components = [];
    const body = [];
    components.push(<FormControl componentClass="select" placeholder="select" name="tutor"></FormControl>);
    body.push(<div>{components}</div>);
    return body;
  }

  toggleEdit(event){
    var user = event.currentTarget.id;
    console.log(user);
    this.setState({
      edit: true,
      userId: user
    });
    var ClienteQuery = new Parse.Query(Parse.User);
    ClienteQuery.get(user).then((Cliente)=>{
      var Utente = JSON.parse(JSON.stringify(Cliente));
      console.log(this.state);
      for(var key in Utente){
        //console.log(key);
        this.setState({
          [key]:Utente[key]
        });
      }
    });
    
   
  }

  componentDidUpdate(){
    //console.log(this.state);
    if(this.state.nome != null && !this.state.edit && !this.state.show){
      for(var key in this.state){
        if(key !== "show" && key !== "edit"){
          this.setState({[key]: null})
        }
      }
    }
  }
  closeCallback(){
    this.setState({
      edit:false,
      show:false
    });
  }
  render() {
    return (
      <div>
      <ModalCustom buttonName="Aggiungi Cliente" title="Aggiungi Cliente" class="Clienti" 
      body={this.formComponents()} 
      handleChange={this.handleElementChange.bind(this)} 
      handleSubmit={this.handleSubmit.bind(this)} 
      editMode={this.state.edit} 
      closeCallback={this.closeCallback.bind(this)} 
      showCallback={this.handleShow.bind(this)}
      userId={this.state.userId} 
      resetCallback={this.resetState.bind(this)}
      />
      <Card>
      <CardHeader color="primary">
           <h3 className="Utenti">Clienti</h3>
      </CardHeader>
      <CardBody>
      <TableUtenti group="4" edit={this.toggleEdit.bind(this)} popover={this.assignTutor.bind(this)} submit={this.handleSubmit}/>
      </CardBody>
    </Card>
    </div>
    );
  }
    
    
}

export default withStyles(generalStyle)(Clienti);
 {/* 
*/ }