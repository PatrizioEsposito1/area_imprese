import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import { FormControl, Row, Col, Grid } from 'react-bootstrap';
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import generalStyle from "assets/jss/material-dashboard-react/views/generalStyle.jsx";
import ModalCustom from "components/Modal/ModalCustom.jsx";
import TableUtenti from "components/TableUtenti/TableUtenti.jsx";

import Parse from "parse";

const style = {
  typo: {
    paddingLeft: "25%",
    marginBottom: "40px",
    position: "relative"
  },
  note: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    bottom: "10px",
    color: "#c0c1c2",
    display: "block",
    fontWeight: "400",
    fontSize: "13px",
    lineHeight: "13px",
    left: "0",
    marginLeft: "20px",
    position: "absolute",
    width: "260px"
  },
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const initial = {
  show: false,
  edit: false,
  user: null
}
class Operatori extends React.Component{

  constructor(props, context){
    super(props, context);
    this.handleShow = this.handleShow.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.formComponents = this.formComponents.bind(this);
    this.handleElementChange = this.handleElementChange.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      show: false,
      edit: false,
      user: null
    };
  }
  
  handleElementChange(event){
    const target = event.target;
    const name = target.name;
    const value = target.value;

    this.setState({[name]:value});
  }

  formComponents(){
    const components = [];
    const body = [];
    console.log(this.state);
    components.push(<Row className="show-grid"><Col xs={12} md={12}><h4>Anagrafica Operatore:</h4></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="nome">Nome:</label><FormControl name="nome" onChange={this.handleElementChange} value={this.state.nome}></FormControl></Col><Col xs={6} md={6}><label htmlFor="cognome">Cognome</label><FormControl name="cognome" onChange={this.handleElementChange} value={this.state.cognome}></FormControl></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label htmlFor="email">E-Mail:</label><FormControl name="username" onChange={this.handleElementChange} value={this.state.username}></FormControl></Col><Col xs={6} md={6}><label htmlFor="password">Password:</label><FormControl name="password" type="password" onChange={this.handleElementChange} value={this.state.password}></FormControl></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={6} md={6}><label>Mansione</label>
    <FormControl name="mansione" componentClass="select" onChange={this.handleElementChange} value={this.state.mansione}>
    {this.state.mansioni}
    </FormControl></Col></Row>);

    body.push(<div><Grid>{components}</Grid></div>);
    return body;
  }
  
  componentDidUpdate(){
    if(this.state.nome != null && !this.state.edit && !this.state.show){
      for(var key in this.state){
        if(key != "show" && key != "edit"){
          this.setState({[key]: null})
          this.getMansioni();
        }
      }
    }
  }
  handleClose() {
    this.setState({
      show:false
    });
    
    ModalCustom.handleClose();
  }

  handleShow() {
    this.setState({
       show:true
    });
  }

  closeCallback(){
    this.setState({
      edit:false,
      show:false
    });
  }

  resetState(){
    this.state = initial;
  }

  toggleEdit(event){
    var user = event.currentTarget.id;
    console.log(user);
    this.setState({
      edit: true,
      userId: user
    });
    var TutorQuery = new Parse.Query(Parse.User);
    TutorQuery.get(user).then((Tutor)=>{
      var Utente = JSON.parse(JSON.stringify(Tutor));
      console.log(this.state);
      for(var key in Utente){
        //console.log(key);
        if(key === "mansione"){
          console.log(Utente[key]['objectId']);
            this.setState({
              [key]:Utente[key]
            });
        }else{
          this.setState({
            [key]:Utente[key]
          });
        }
      }
    });
  }
  handleSubmit(){
    if(this.state.edit == false){
      var Cliente = new Parse.User;
      for(var key in this.state){
        if(key != "show" && key != "edit" && key != "mansioni" && key != "user"){
          Cliente.set(key, this.state[key]);
        }
      }
      Cliente.set("username", this.state.username);
      Cliente.set("group", "2");
      
      Cliente.signUp({useMasterKey:true}).then((obj)=>{
          alert("Operatore " + this.state.nome + " " + this.state.cognome + " registrato con successo");
          window.location.reload();
          
      }, (err)=>{
          alert("Errore " + err.message);
      }); 
    }else{
      const storedUser = this.state.userId;
      const ClienteQuery = new Parse.Query(Parse.User);
      ClienteQuery.get(storedUser).then((Cliente)=>{
          for(var key in this.state){
            if(key != "show" && key != "edit" && key != "sessionToken" && key != "mansioni"){
              Cliente.set(key, this.state[key]);
            }
          }
          Parse.Object.saveAll([Cliente], {useMasterKey:true}).then((res)=>{
               alert("Utente modificato con successo");
          });
      });
    }
    
  }
  componentDidMount(){
    this.getMansioni();
  }

  getMansioni = () =>{
    Parse.Config.get('Mansioni').then((res)=>{
      const mansioni = [];
       const arrayMansioni = res.get('Mansioni');
       mansioni.push(<option>Scegli mansione</option>);
       for(var i = 0; i<arrayMansioni.length; i++){
        const mansione = arrayMansioni[i];
        console.log(mansione);
        mansioni.push(<option value={mansione}>{mansione}</option>);
       }
       this.setState({
         mansioni: mansioni
       });
    });

  }
  render(){
    return(
      <div>
      <ModalCustom buttonName="Aggiungi Operatore" title="Aggiungi Operatore" class="Utente" 
      body={this.formComponents()} 
      handleChange={this.handleElementChange.bind(this)} 
      handleSubmit={this.handleSubmit.bind(this)} 
      editMode={this.state.edit} 
      closeCallback={this.closeCallback.bind(this)} 
      showCallback={this.handleShow.bind(this)}
      userId={this.state.userId} 
      resetCallback={this.resetState.bind(this)}
      />
      <Card>
      <CardHeader color="primary">
           <h3 className="Utenti">Operatori</h3>
      </CardHeader>
      <CardBody>
      <TableUtenti group="2" edit={this.toggleEdit.bind(this)}  submit={this.handleSubmit}/>
      </CardBody>
    </Card>
    </div>
    );
  }
}
export default withStyles(style)(Operatori);
