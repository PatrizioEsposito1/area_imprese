import React from "react";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import { FormControl, Row, Col, Grid } from 'react-bootstrap';
import generalStyle from "assets/jss/material-dashboard-react/views/generalStyle.jsx";
import ModalCustom from "components/Modal/ModalCustom.jsx";
import TableComunicazioni from "components/TableComunicazioni/TableComunicazioni.jsx";

//Parse components
import Parse from "parse";

const initial = {
  show:false,
  edit:false,
  user:null
}
class Comunicazioni extends React.Component{
  constructor(props, context){
    super(props, context);
    this.handleShow = this.handleShow.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.formComponents = this.formComponents.bind(this);
    this.handleElementChange = this.handleElementChange.bind(this);
    this.state = {
      show: false,
      edit: false,
      user: null,
      assign: false
    };
  }
  
  handleElementChange(event){
    const target = event.target;
    const name = target.name;
    const value = target.value;

    this.setState({[name]:value});
  }

  formComponents(){
    const components = [];
    const body = [];
    components.push(<Row className="show-grid"><Col xs={12} md={12}><h4>Dettagli comunicazione:</h4></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={12} md={12}><label htmlFor="titolo">Titolo comunicazione:</label><FormControl name="titolo" onChange={this.handleElementChange} value={this.state.titolo}></FormControl></Col></Row>);
    components.push(<Row className="show-grid"><Col xs={12} md={12}><label htmlFor="corpo">Corpo comunicazione:</label><FormControl name="corpo" componentClass="textarea" onChange={this.handleElementChange} value={this.state.corpo} style={{height:250}}></FormControl></Col></Row>);
    body.push(<div><Grid>{components}</Grid></div>);
    return body;
  }
  componentDidUpdate(){
    //console.log(this.state);
    if(this.state.titolo != null && !this.state.edit && !this.state.show){
      for(var key in this.state){
        if(key != "show" && key != "edit"){
          this.setState({[key]: null})
        }
      }
    }
  }
  handleClose() {
    this.setState({
      show:false
    });
    ModalCustom.handleClose();
  }

  handleShow() {
    this.setState({
       show:true
    });
  }

  closeCallback(){
    this.setState({
      edit:false,
      show:false
    });
  }

  resetState(){
    this.state = initial;
  }

  toggleEdit(event){
    var id = event.currentTarget.id;
    this.setState({
      edit: true,
      userId: id
    });
    var ComunicazioneQuery = new Parse.Query("Comunicazioni");
    ComunicazioneQuery.get(id).then((comunicazione)=>{
      var Comunicazione = JSON.parse(JSON.stringify(comunicazione));
      console.log(this.state);
      for(var key in Comunicazione){
        //console.log(key);
        this.setState({
          [key]:Comunicazione[key]
        });
      }
    });
  }
  handleSubmit(){
    if(this.state.edit == false){
      var ComunicazioneClass = Parse.Object.extend("Comunicazioni");
      var Comunicazione = new ComunicazioneClass();
      for(var key in this.state){
        if(key != "show" && key != "edit"){
          Comunicazione.set(key, this.state[key]);
        }
      }
      Comunicazione.set("author", Parse.User.current());
      
      Parse.Object.saveAll([Comunicazione], {useMasterKey:true}).then((obj)=>{
          alert("Comunicazione '" + this.state.titolo + "' aggiunta con successo");
          window.location.reload();
          
      }, (err)=>{
          alert("Errore " + err.message);
      }); 
    }else{
      const comunicazioneId = this.state.userId;
      const ComunicazioneQuery = new Parse.Query("Comunicazione");
      ComunicazioneQuery.get(comunicazioneId).then((Comunicazione)=>{
          for(var key in this.state){
            if(key != "show" && key != "edit" && key != "sessionToken"){
              Comunicazione.set(key, this.state[key]);
            }
          }
          Parse.Object.saveAll([Comunicazione], {useMasterKey:true}).then((res)=>{
               alert("Comunicazione modificato con successo");
          });
      });
    }
    
  }

  render(){
    return (
        <div>
        <ModalCustom buttonName="Aggiungi Comunicazione" title="Aggiungi Comunicazione" class="Comunicazione" 
      body={this.formComponents()} 
      handleChange={this.handleElementChange.bind(this)} 
      handleSubmit={this.handleSubmit.bind(this)} 
      editMode={this.state.edit} 
      closeCallback={this.closeCallback.bind(this)} 
      showCallback={this.handleShow.bind(this)}
      userId={this.state.userId} 
      resetCallback={this.resetState.bind(this)}
      />
        <Card>
      <CardHeader color="primary">
           <h3 className="Utenti">Comunicazioni</h3>
      </CardHeader>
      <CardBody>
      <TableComunicazioni edit={this.toggleEdit.bind(this)} submit={this.handleSubmit}/>
      </CardBody>
    </Card>
        </div>

    );
  }
}


export default withStyles(generalStyle)(Comunicazioni);
