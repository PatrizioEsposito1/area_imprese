import React from "react";
import PropTypes from "prop-types";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// @material-ui/icons
import Note from "@material-ui/icons/Note";
// core components


import Parse from "parse";
import { Redirect } from 'react-router-dom';
import Paper from '@material-ui/core/Paper'; 

const styles ={
     head:{
       backgroundColor: '#4ac8ed',
       color: 'white'
     },
     row:{
       color:'#fff'
     },
     cell:{
       color:'#fff'
     }
}
class Dashboard extends React.Component {
  state = {
    value: 0,
    redirect:false
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  setRedirect = () => {
    this.setState({
        redirect: true
    })
}
renderRedirect = () => {
      return <Redirect to='/login' />
  }
  checkUserLogon = () =>{
    if(Parse.User.current() == null){
      this.setState({
         redirect:true
      });
      return false;
    }
    return true;
  }

  renderPage = () => {
  if(Parse.User.current() != null || Parse.User.current() != undefined){

  
    if(Parse.User.current().get('group') === "1" ){
    return( <div><h2>Dashboard amministrativa</h2></div>);
    }else if(Parse.User.current().get('group') === "4"){
      //Clienti
      
      return(<div>
            <h2>Bacheca comunicazioni</h2>
      <Paper>
      <Table>
        <TableHead className={this.props.classes.head}>
          <TableRow className={this.props.classes.row}><TableCell className={this.props.classes.cell}>Titolo</TableCell><TableCell className={this.props.classes.cell}>Corpo</TableCell><TableCell className={this.props.classes.cell}>Azioni</TableCell></TableRow>
        </TableHead>
        <TableBody>
          {this.state.comunicazioni}
        </TableBody>
      </Table>
      </Paper>
      </div>)
    }
  }
}

  getExcerpt(corpo){
    var excerpt = corpo.substring(0, 120) + "..."; 
    return excerpt;
  }
  getComunicazioni = () => {
    var Query = new Parse.Query('Comunicazioni');
    Query.find().then((Comunicazioni)=>{
      const comunicazioni_array = [];
      for(var i = 0; i<Comunicazioni.length; i++){
        var Comunicazione = Comunicazioni[i];
        const titolo = Comunicazione.get('titolo');
        const corpo = Comunicazione.get('corpo');
        const excerpt = this.getExcerpt(corpo);
        comunicazioni_array.push(<TableRow><TableCell>{titolo}</TableCell><TableCell>{excerpt}</TableCell><TableCell><Note id={Comunicazione.id} /></TableCell></TableRow>)
      }
         this.setState({
           'comunicazioni': comunicazioni_array
         });
    });
  }
  componentDidMount = () =>{
    this.checkUserLogon();
    this.getComunicazioni();
  }
  render() {
    const { classes } = this.props;
    return (
      
      <div>
      {
        this.checkUserLogon() ? null: this.renderRedirect()
      }
      {
        this.renderPage()
      }
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Dashboard);
