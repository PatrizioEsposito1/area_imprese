import React from "react";
import ReactDOM from "react-dom";
import Parse from "parse";
import withStyles from "@material-ui/core/styles/withStyles";
import DropIn from "braintree-web-drop-in-react";
import Paper from '@material-ui/core/Paper'; 
import { FormControl, Grid, Row, Col } from "react-bootstrap";
import Button from "@material-ui/core/Button";
import PaypalButton from "components/PaypalButton/PaypalButton.jsx";

const styles = {
      Container:{
          padding:35
      },
      paypalButton:{
          marginTop:15
      },
      rowLeft:{
          float: 'left'
      }
}


class Plafond extends React.Component{
    instance;

    state = {
        clientToken: null
    };
    
    handleElementChange = (event) => {
        this.setState({
            [event.target.name]:event.target.value
        });
    }
    componentDidMount = () => {
        console.log("PLAFOND ATTIVO");
        {window.recordUserId(Parse.User.current().id)}
        try{
            {window.renderButton()}
        }catch(err){
            window.location.reload();
        }
        
    }


    render(){
        const CLIENT = {
            sandbox: 'AfkBI0MuZivVn6ej6nPda1acQ4J3uw8j5D-sWdP23uYZC36uIGM0DYNNcMGdsoo9HalSbeoyva1suxMm',
            production: 'ARn3wluwjFvymw5qW-BJslDyMNLktLt9bfy5eeDd0NdNNaaXy9sdmkZ3ZJEbGvzLkOw46uZNsL19dtK6'
      };
        return(
            <div>
                <Grid className={this.props.rowLeft}>
                <h1>Acquista crediti</h1>
                <Paper className={this.props.classes.Container}>
                <h2>Effettua pagamento</h2>
                <Row>
                <Col xs={3} md={3}>
                <label>Crediti desiderati</label> 
                <FormControl name="amount" value={this.state.amount} onChange={this.handleElementChange} id="paypal-amount"></FormControl>
                </Col>
                
               </Row>
               <div id="paypal-button-container" className={this.props.paypalButton}>
               
                </div>
               </Paper>
               </Grid>
               
        
            </div>
        )
    }
}

export default withStyles(styles)(Plafond)