import Dashboard from "@material-ui/icons/Dashboard";
import EuroSymbol from "@material-ui/icons/EuroSymbol";

import DashboardPage from "views/Dashboard/Dashboard.jsx";
import Plafond from "views/Plafond/Plafond.jsx";
import Ordini from "views/Ordini/Ordini.jsx";
const clientiRoutes = [
    {
      path: '/dashboard',
      sidebarName: 'Pagina principale',
      navbarName: 'Pagina principale',
      icon: Dashboard,
      component: DashboardPage
    },
    {
      path: '/plafond',
      sidebarName: 'Gestione plafond',
      navbarName: 'Gestione plafond',
      icon: EuroSymbol,
      component: Plafond
    },
    { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
]

export default clientiRoutes;