// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
// import ContentPaste from "@material-ui/icons/ContentPaste";
import Flag from "@material-ui/icons/Flag";
import ShoppingCart from "@material-ui/icons/ShoppingCart";
import School from "@material-ui/icons/School";
import Face from "@material-ui/icons/Face";
import ShoppingBasket from "@material-ui/icons/ShoppingBasket";
import EuroSymbol from "@material-ui/icons/EuroSymbol";
import Store from "@material-ui/icons/Store";
// core components/views
import DashboardPage from "views/Dashboard/Dashboard.jsx";
import Clienti from "views/Clienti/Clienti.jsx";
import Tutors from "views/Tutors/Tutors.jsx";
import Operatori from "views/Operatori/Operatori.jsx";
import Comunicazioni from "views/Comunicazioni/Comunicazioni.jsx";
import Prodotti from "views/Prodotti/Prodotti.jsx";
import Ordini from "views/Ordini/Ordini.jsx";
import Plafond from "views/Plafond/Plafond.jsx";
import Catalogo from "views/Catalogo/Catalogo.jsx";

const dashboardRoutes = [
  {
    path: "/dashboard",
    sidebarName: "Resoconto",
    navbarName: "Resoconto",
    group: "1",
    icon: Dashboard,
    component: DashboardPage
  },
  {
    path: "/dashboard",
    sidebarName: "Bacheca",
    navbarName: "Bacheca",
    group: "4",
    icon: Dashboard,
    component: DashboardPage
  },
  {
    path: "/clienti",
    sidebarName: "Clienti",
    navbarName: "Clienti",
    group: "1",
    icon: Person,
    component: Clienti
  },
  {
    path: "/tutors",
    sidebarName: "Tutors",
    navbarName: "Tutors",
    group: "1",
    icon: School,
    component: Tutors
  },
  {
    path: "/operatori",
    sidebarName: "Operatori",
    navbarName: "Operatori",
    group: "1",
    icon: Face,
    component: Operatori
  },
  {
    path: "/comunicazioni",
    sidebarName: "Comunicazioni",
    navbarName: "Comunicazioni",
    group: "1",
    icon: Flag,
    component: Comunicazioni
  },
  {
    path: "/prodotti",
    sidebarName: "Prodotti",
    navbarName: "Prodotti",
    group: "1",
    icon: ShoppingBasket,
    component: Prodotti
  },
  {
    path: "/ordini",
    sidebarName: "Ordini",
    navbarName: "Ordini",
    group: "1",
    icon: ShoppingCart,
    component: Ordini
  },
  {
    path: "/plafond",
    sidebarName: "Plafond",
    navbarName: "Plafond",
    group: "4",
    icon: EuroSymbol,
    component: Plafond
  },
  {
    path: '/catalogo',
    sidebarName: 'Catalogo',
    navbarName: 'Catalogo',
    group: '4',
    icon: Store,
    component: Catalogo
  },
  { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default dashboardRoutes;
