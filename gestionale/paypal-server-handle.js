var express = require('express');
var app = express();
var Parse = require('parse/node');
var cors = require('cors');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
Parse.initialize("Area Imprese", null, "1234567");
Parse.serverURL = "http://138.68.100.112:3131/parse";
Parse.masterKey = "1234567";

app.post('/registerTransaction', function(request, response){
      var id = request.body['userId'];
      var TransactionClass = Parse.Object.extend('PaypalTransaction');
      var Transaction = new TransactionClass();
      var UserQuery = new Parse.Query(Parse.User);
      UserQuery.get(id).then((User)=>{
          Transaction.set('user', User);
          Transaction.set('order_id', request.body['orderId']);
          Transaction.set('quantita', request.body['amount']);
          Transaction.set('type', 'Paypal');
          Parse.Object.saveAll([Transaction], {useMasterKey:true}).then((res)=>{
               User.set('crediti', parseInt(request.body['amount']) + User.get('crediti'));
               Parse.Object.saveAll([User], {useMasterKey : true}).then((res)=>{
                    response.send("OK");
               });
          });
      }); 
});


app.listen(9001, function(res){
    
});